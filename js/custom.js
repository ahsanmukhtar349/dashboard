$(".startopion .fa-angle-down").click(function(){
$('.displayonclick').toggleClass('showinfo');
$('.startopion h2').toggleClass('addstyle');
})


$('.enableforshow').click(function(){
$('.sub-tabs').toggleClass('showtabs');

$('.enablenotice').toggleClass('displaynono');
})

$('.settingtab .select1').click(function(){
    $('.settingtab .select1').toggleClass('changeimg');
})

$('.settingtab .select2').click(function(){
    $('.settingtab .select2').toggleClass('changeimg');
})

$('.selectpage').click(function(){
    $('.selectpage').toggleClass('changeimg');
})
$('.selectWidget').click(function(){
    $('.selectWidget').toggleClass('changeimg');
})
$('.selectAlgorithm').click(function(){
    $('.selectAlgorithm').toggleClass('changeimg');
})
$('.norecommendation').click(function(){
    $('.norecommendation').toggleClass('changeimg');
})
$('.themedropdown').click(function(){
    $('.themedropdown').toggleClass('changeimg');
})
$('.locationdropdown').click(function(){
    $('.locationdropdown').toggleClass('changeimg');
})
$('.fontdropdown').click(function(){
    $('.fontdropdown').toggleClass('changeimg');
})



$(document).ready(function() {
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});




$(document).ready(function(){

   
    
    $(".with-color").click(function () {    
       if($(this).hasClass("copybtn"))
       {
       		$(this).addClass("copybtncolor");
       		$(this).removeClass("copybtn");
       }
       else{
       		$(this).addClass("copybtn");
       		$(this).removeClass("copybtncolor");
       }
    });

    $(".pushme2").click(function(){
		$(this).text(function(i, v){
		   return v === 'Copy' ? 'Copied' : 'Copy'
		});
    });
});
// showtabs
// displaynono
// $(".detailpage").hide();
$(".floatright a").click(function(){
    $(".detailpage").addClass("showtabs");
    $(".detailpage").removeClass("displaynono");
    $(".sub-tabs").addClass("displaynono");
    $(".forenable").addClass("displaynono");
});

$(".cancelbtn").click(function(){
    $(".detailpage").removeClass("showtabs");
    $(".detailpage").addClass("displaynono");
    $(".sub-tabs").removeClass("displaynono");
    $(".forenable").removeClass("displaynono");
    
});

$(".finishbtn").click(function(){
    $(".detailpage").removeClass("showtabs");
    $(".detailpage").addClass("displaynono");
    $(".sub-tabs").removeClass("displaynono");
    $(".forenable").removeClass("displaynono");
});


$(function() {
    $('#themeselector').change(function(){
      $('.Previewgallery').hide();
      $('#' + $(this).val()).show();
    });
  });